import React, { Component } from 'react';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Main from './components/Main';
import Mundan from './components/Mundan';
import Family from './components/Family';
import { BrowserRouter as Router, Route } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route path="/" exact component={Main} />
          <Route path="/mundan/" component={Mundan} />
          <Route path="/family/" component={Family} />
        </div>
      </Router>
    );
  }
}

export default App;
