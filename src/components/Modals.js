import React from 'react';
import Modal from 'react-modal';
import { customStyles, modalContent } from './style';

const Modals = ({ isOpen, closeModal, month, data }) => (
  <Modal isOpen={isOpen} style={customStyles}>
    <div className={modalContent}>
      <div className="modalHeader">
        <button
          onClick={closeModal}
          className="closeBtn"
          dangerouslySetInnerHTML={{ __html: `&#10005;` }}
        />
      </div>
      <div className="boxModal">
        <div className="boxImg">
          <img src={data.cakeImage} alt="cake" className="modalImg" />
        </div>
        <div className="boxImg">
          <img src={data.monthImage} alt="month Pic" className="modalImg" />
        </div>
        <div className="fontSubSize color_purple fontFamily fontBold">{data.achivement}</div>
      </div>
    </div>
  </Modal>
);

export default Modals;
