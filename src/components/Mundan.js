import React from 'react';
//import PropTypes from 'prop-types';
//import Mundangallery from './Mundangallery';
// import Gallery from 'react-grid-gallery';
import Gallery from './Gallery';
//import IMAGES from './Mundangallery';

class Mundan extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div
        className="container-fluid backgroundImg fontBold fontFamily"
        style={{
          display: 'block',
          minHeight: '100px',
          width: '100%',
          //border: '4px solid black',
          overflow: 'auto',
          textAlign: 'center'
        }}
      >
        <h1 className="text-center color_purple">Mundan Ceremony</h1>
        <h2 className="text-center color_purple">On 28th March 2018</h2>
        <Gallery />
      </div>
    );
  }
}

// Mundan.propTypes = {};

// Mundan.defaultProps = {};

export default Mundan;
