import React from 'react';
import { Link } from 'react-router-dom';

const Rsidebar = props => {
  // const redirectToMundan = () => {
  //   location.replace('/mundan'); //eslint-disable-line
  // };

  return (
    <div className="col-lg-2 col-sm-12 ">
      <ul className="list-unstyled fontSubSize ">
        <li className="my-3 color_purple divNoRadius lihover" tabIndex="2">
          <Link to="/family" className="color_purple">
            My Family
          </Link>
        </li>
        <li className="my-3 color_purple divNoRadius lihover" tabIndex="2">
          First trip to India...
        </li>
        <li className="my-3 color_purple divNoRadius lihover" tabIndex="2">
          <Link to="/mundan" className="color_purple">
            Mundan Ceremony
          </Link>
        </li>
        <li className="my-3 color_purple divNoRadius lihover" tabIndex="2">
          My first road trip...
        </li>
        <li className="my-3 color_purple divNoRadius lihover" tabIndex="2">
          I love to..eat Dance
        </li>

        <li className="my-3 color_purple divNoRadius lihover" tabIndex="2">
          Some of my Modelling
        </li>
      </ul>
    </div>
  );
};
export default Rsidebar;
