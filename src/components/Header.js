import React from 'react';

const Header = props => {
  return (
    <div className="col-12 text-center">
      <h1 className="color_purple">You are my Sunshine....</h1>
      <h2 className="color_purple">My only Sunshine....</h2>
    </div>
  );
};

export default Header;
