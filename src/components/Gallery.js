import React from 'react';
import PropTypes from 'prop-types';
import { css } from 'emotion';
import IMAGES from './Mundangallery';

const imgContainer = css`
  width: 80vw;
  margin: 0 auto;

  .img-wrapper {
    display: inline-block;
    width: 19%;
    margin: 0.5%;

    img {
      width: 100%;
    }
  }
`;

const Gallery = props => {
  const list = Array(20).fill(null);
  const getRandom = (min, max) => Math.ceil(Math.random() * (max - min) + min);

  return (
    <div className={imgContainer}>
      {IMAGES.map(({ src, caption }) => (
        <div className="img-wrapper">
          <img className="divRadius" src={src} alt={caption} />
        </div>
      ))}
    </div>
  );
};

Gallery.propTypes = {};

Gallery.defaultProps = {};

export default Gallery;
