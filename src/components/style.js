import styled from 'styled-components';
import { css } from 'emotion';

export const customStyles = {
  content: {
    border: '2px solid black',
    borderradius: '25px',
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

export const modalContent = css`
  width: 80vw;

  .modalHeader {
    text-align: right;

    .closeBtn {
      font-size: 1.5rem;
      font-weight: bold;
      border: none;
      background-color: transparent;
    }
  }
`;
