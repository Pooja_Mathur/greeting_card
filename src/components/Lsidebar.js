import React from 'react';
import Modals from './Modals';
import json from './modal.json';

class Lsidebar extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      showModal: false,
      dataToSend: 'This data is being sent to the modal'
    };

    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
  }

  openModal(e) {
    this.setState({
      showModal: true,
      dataToSend: json.data[e.target.id]
    });
  }

  closeModal() {
    this.setState({
      showModal: false
    });
  }

  render() {
    return (
      <div className="col-lg-2 col-sm-12">
        <ul className="list-unstyled fontSubSize ">
          {Object.keys(json.data).map(month => (
            <li
              id={month}
              onClick={this.openModal}
              key={month}
              // onClick={this.openModal}
              className="my-3 color_purple divRadius liHover"
              tabIndex="2"
            >
              {month} Month
            </li>
          ))}
        </ul>

        <Modals
          isOpen={this.state.showModal}
          closeModal={this.closeModal}
          month="test"
          data={this.state.dataToSend}
        />
      </div>
    );
  }
}

export default Lsidebar;
