import React from 'react';
//import PropTypes from 'prop-types';
import Header from './Header';
import Lsidebar from './Lsidebar';
import Article from './Article';
import Rsidebar from './Rsidebar';
import Footer from './Footer';

const Main = props => {
  return (
    <div className="container-fluid backgroundImg fontBold fontFamily">
      <div className="row">
        <Header />
      </div>
      <div className="row">
        <Lsidebar />
        <Article />
        <Rsidebar />
      </div>
      <div className="row">
        <Footer />
      </div>
    </div>
  );
};

Main.propTypes = {};

Main.defaultProps = {};

export default Main;
