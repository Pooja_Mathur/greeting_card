//import React from 'react';
//import { render } from 'react-dom';
//import Gallery from 'react-grid-gallery';

const IMAGES = [
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0024.jpg',
    thumbnail: 'http://i.puza.in/mundan/c2.jpg',
    thumbnailWidth: 183,
    thumbnailHeight: 326,
    isSelected: true,
    caption: 'After Rain (Jeshu John - designerspics.com)'
  },
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0030.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0030.jpg',
    thumbnailWidth: 320,
    // thumbnailHeight: 174,
    isSelected: false,
    caption: 'After Rain (Jeshu John - designerspics.com)'
  },
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0025.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0025.jpg',
    thumbnailWidth: 320,
    // thumbnailHeight: 174,
    isSelected: false,
    caption: 'After Rain (Jeshu John - designerspics.com)'
  },
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0034.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0034.jpg',
    thumbnailWidth: 320,
    // thumbnailHeight: 174,
    isSelected: false,
    caption: 'After Rain (Jeshu John - designerspics.com)'
  },
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0036.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0036.jpg',
    thumbnailWidth: 320,
    // thumbnailHeight: 174,
    isSelected: false,
    caption: 'After Rain (Jeshu John - designerspics.com)'
  },
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0043.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0043.jpg',
    thumbnailWidth: 320,
    // thumbnailHeight: 174,
    isSelected: false,
    caption: 'After Rain (Jeshu John - designerspics.com)'
  },
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0050.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0050.jpg',
    thumbnailWidth: 320,
    // thumbnailHeight: 174,
    isSelected: false,
    caption: 'After Rain (Jeshu John - designerspics.com)'
  },
  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0063.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0063.jpg',
    // thumbnailWidth: 320,
    // thumbnailHeight: 212,
    tags: [{ value: 'Ocean', title: 'Ocean' }, { value: 'People', title: 'People' }],
    caption: 'Boats (Jeshu John - designerspics.com)'
  },

  {
    src: 'http://i.puza.in/mundan/IMG-20180323-WA0077.jpg',
    thumbnail: 'http://i.puza.in/mundan/IMG-20180323-WA0077.jpg'
    // thumbnailWidth: 320,
    // thumbnailHeight: 212
  }
];

export default IMAGES;
