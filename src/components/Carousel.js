import React from 'react';
import Slider from 'react-slick';
import styled from 'styled-components';

const NewComponent = styled.div`
  width: 100%;
  height: 600px;
  background-color: #FFCCCC;
  background-image url(${({ image }) => image});
  background-position: center center;
  background-size: contain;
  background-repeat: no-repeat;
  position: relative;
  border:4px solid black;
  border-radius:30px;
`;

const Description = styled.div`
  position: absolute;
  bottom: 0px;
  width: 100%;
  text-align: center;
  padding: 0.5rem 0;
  background-color: white;
  border-radius: 30px;
  font-size: 1.75rem;
  .sub {
    color: purple;
  }
`;

const Carousel = ({ data }) => {
  return (
    <div
      style={{
        width: '100%'
      }}
    >
      <Slider
        {...{
          autoplay: true,
          autoplaySpeed: 2000,
          dots: true,
          infinite: true,
          speed: 300,
          slidesToShow: 1,
          slidesToScroll: 1,
          adaptiveHeight: true
        }}
      >
        {data.map(({ image, description }) => (
          <NewComponent image={image}>
            <Description>
              <span>My First </span>
              <span className="sub">{description}</span>
            </Description>
          </NewComponent>
        ))}
      </Slider>
    </div>
  );
};

export default Carousel;
