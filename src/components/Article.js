import React from 'react';
import Slider from 'react-slick';
import * as img1 from '../images/carousel/smile.jpg';
import * as img2 from '../images/carousel/4.jpg';
import * as img3 from '../images/carousel/3.jpg';
import * as img4 from '../images/carousel/nanu1.jpg';
import * as img5 from '../images/carousel/dadi.JPG';
import * as img6 from '../images/carousel/winters.JPG';
import * as img7 from '../images/carousel/HC.jpg';
import * as img8 from '../images/carousel/toy2.JPG';
import * as img9 from '../images/carousel/swings.jpg';
import Carousel from './Carousel';

const Article = props => {
  const data = [
    {
      image: img1,
      description: 'Smile'
    },
    {
      image: img2,
      description: 'Hug with PAPA'
    },
    {
      image: img3,
      description: 'Hug with MIMI'
    },
    {
      image: img4,
      description: 'Hug with Nanu'
    },
    {
      image: img5,
      description: 'Hug with Dadi & Dadu'
    },
    {
      image: img6,
      description: 'Winters'
    },
    {
      image: img7,
      description: 'High chair'
    },
    {
      image: img8,
      description: 'Toy'
    },
    {
      image: img9,
      description: 'Swing'
    }
  ];

  return (
    <div className="col-lg-8 col-sm-12">
      <div className="row">
        <div className="col-lg-8 offset-lg-2 col-sm-12">
          <Carousel data={data} />
        </div>
      </div>
    </div>
  );
};

export default Article;
